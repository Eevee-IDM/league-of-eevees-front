// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
    require('karma-jasmine'),
    require('karma-chrome-launcher'),
    require('karma-jasmine-html-reporter'),
    require('karma-coverage-istanbul-reporter'),
    require('karma-coverage'),
    require('karma-junit-reporter'),
    require('karma-phantomjs-launcher'),
    require('@angular/cli/plugins/karma')
    ],
    client:{
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly' ],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: ['progress', 'coverage', 'dots', 'junit'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,

    coverageReporter: {
      type: 'lcov',
      dir: 'karma-results/coverage',
      subdir: '.'
    },

    junitReporter: {
      outputDir: 'karma-results',
      outputFile: 'karma-results.xml',
      xmlVersion: 1
    },

    browsers: ['PhantomJS'],

    singleRun: true
  });
};
