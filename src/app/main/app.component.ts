import { Component } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoggedIn : boolean = false;
  isShowingMenu : boolean = true;

  constructor(private userService: UserService) {
    this.isLoggedIn = this.userService.isLoggedIn();
  }

  logOut() {
    this.userService.logOutUser();
    window.location.reload();
  }

  switchMenu() {
    this.isShowingMenu = !this.isShowingMenu;
  }

}
