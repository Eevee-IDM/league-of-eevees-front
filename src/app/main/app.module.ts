import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AceEditorModule } from 'ng2-ace-editor';
import { DragulaModule } from "ng2-dragula";

/* --- COMPONENTS --- */
import { AppComponent } from './app.component';
import { DiagramComponent } from '../diagram/diagram.component';
import { LandingPageComponent } from '../landing-page/landing-page.component';
import { TextEditorComponent } from '../text-editor/text-editor.component';
import { GridComponent } from '../grid/grid.component';
import { MenuComponent } from '../menu/menu.component';
import { LoadingComponent } from '../loading/loading.component';
import { FavoritesComponent } from '../favorites/favorites.component';

/* --- SERVICES --- */
import { UserService } from '../services/user.service';
import { ParseDslService } from '../services/parse-dsl.service';
import { DiagramService } from '../diagram/diagram.service';
import { FavoriteService } from '../favorites/favorites.service';

/* --- ROUTING --- */
import { AppRoutingModule } from './app-routing.module';

/* --- CHARTS --- */
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import { FusionChartsModule } from 'angular4-fusioncharts';

FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme);

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    TextEditorComponent,
    LandingPageComponent,
    GridComponent,
    DiagramComponent,
    MenuComponent,
    FavoritesComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AceEditorModule,
    HttpModule,
    DragulaModule,
    FusionChartsModule
  ],
  providers: [UserService, {provide: LocationStrategy, useClass: HashLocationStrategy}, 
              DiagramService, ParseDslService, FavoriteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
