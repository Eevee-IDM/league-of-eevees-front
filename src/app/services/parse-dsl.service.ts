import { Injectable, } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class ParseDslService {
    private url = "http://ip94.ip-137-74-154.eu/gloe";
    // private url = "localhost:5150/gloe";

    constructor(private http: Http) { }
    
    query(request: string) {
        console.log("I'm gonna fetch answers for :", request);
        let params = new URLSearchParams();
        params.set('query', request);
        return this.http.get(this.url, { search: params })
            .map((res:Response) => res.json())
            .catch((err:any) => {
                return Observable.of({
                    code: 500,
                    message: "Unable to interact with the server, please try again later."
                });
            });
    }

    /** 
	* Prints an error log and returns the specified value
	* @param {any} elementToReturn - the element to return, which can be undefined
	*/
	handleError(elementToReturn = undefined) : Observable<any> {
        console.log("An error occurred..."); // needs to be a slight bit more explicit
		return Observable.of(mockData);
	}

}

const mockData = {
    "id": 23467235,
    "axis": {
        "xName": "time",
        "yName": "victories per day"
    },
    "data": [
        {
            "label": 1,
            "value": 12
        },
        {
            "label": 2,
            "value": 7
        },
        {
            "label": 3,
            "value": 5
        },
        {
            "label": 4,
            "value": 0
        },
        {
            "label": 5,
            "value": 4
        },
        {
            "label": 6,
            "value": 4
        }
    ],
    "date": {
        "from": "2018-04-22T06:00:00Z",
        "step": "day",
        "to": "2018-04-27T06:00:00Z"
    },
    "display": {
        "name": "Victories",
        "type": "column2d"
    },
    "dimension": 2,
    "summoner": "name"
}