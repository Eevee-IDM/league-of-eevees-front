import { Injectable, } from '@angular/core';

@Injectable()
export class UserService {
    private storage = window.localStorage;

	  constructor() {
      if (!this.storage.getItem("users"))
        this.storage.setItem("users", "[]");
    }

    /* @return {boolean} Whether the user is logged in or not. */
    isLoggedIn() : boolean {
      let user = this.storage.getItem("currentUser");
      return user ? true : false;
    }


    /* @return the name of the connected summoner. */
    getSummonerName() : string {
      return this.storage.getItem("currentUser");
    }

    /*
    * Set the current user based on the given pseudo and register them
    * if that's the first time they're logging in.
    */
    logInUser(pseudo: string) : void {
      this.setCurrentUser(pseudo);
      this.registerUser(pseudo);
    }

    /*
    * Remove the current user from localStorage.
    */
    logOutUser() : void {
      this.storage.removeItem("currentUser");
    }

    /*
    * Store the given pseudo in localStorage as the current user.
    */
    private setCurrentUser(pseudo: string) : void {
      this.storage.setItem("currentUser", pseudo);
    }

    /*
    * Register the user in the localStorage's users list if it's the first time
    * they're logging in.
    */
    private registerUser(pseudo: string) : void {
      let users = JSON.parse(this.storage.getItem("users"));

      if (users.indexOf(pseudo) == -1) {
        users.push(pseudo);
        this.storage.setItem("users", JSON.stringify(users));
      }
    }

}
