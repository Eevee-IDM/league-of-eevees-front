import { TestBed, inject } from '@angular/core/testing';
import { UserService } from '../services/user.service';

describe('UserService', () => {
  let service;
  let storage = window.localStorage;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [ UserService ]
  }));

  beforeEach(inject([UserService], s => {
    service = s;
  }));

  it("should create a 'users' field in localStorage", () => {
    expect(storage.getItem('users')).not.toBeUndefined();
  });

  it("should not have any 'currentUser' after logging out", () => {
    service.logInUser("piou");
    service.logOutUser();
    expect(storage.getItem('currentUser')).toBeNull();
  });

  it("should tell whether the user is logged in or not", () => {
    service.logInUser("piou");
    expect(service.isLoggedIn()).toBeTruthy();
    service.logOutUser();
    expect(service.isLoggedIn()).toBeFalsy();
  });

  describe('Log in the user', () => {
    it("should save the user in localStorage", () => {
      service.logInUser("piou");
      expect(storage.getItem('currentUser')).toEqual("piou");
    });

    it("should add the user in the localStorage's users list", () => {
      service.logInUser("piou");
      let users = JSON.parse(storage.getItem('users'));
      expect(users).toContain("piou");
    });

    it("should not remove other users from the localStorage's users list", () => {
      service.logInUser("plip");
      service.logInUser("plop");
      service.logInUser("ploup");

      let users = JSON.parse(storage.getItem('users'));
      expect(users).toContain("plip");
      expect(users).toContain("plop");
      expect(users).toContain("ploup");
    });
  });

});
