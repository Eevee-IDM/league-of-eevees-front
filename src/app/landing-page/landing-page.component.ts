import { Component } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent {
  error : boolean = false;

  constructor(private userService: UserService) { }

  /*
  * Log the user with the given pseudo and redirect to the data analysis page.
  */
  logIn(pseudo: string) {
    if (!pseudo) {
      this.error = true;
      return;
    }
    this.userService.logInUser(pseudo);
    window.location.reload();
  }
}
