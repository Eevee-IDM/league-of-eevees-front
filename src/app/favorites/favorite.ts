 	
import { Diagram } from '../diagram/diagram';

export class Favorite {
	title: string;
	imgUrl: string;
	request: string;
	type: string;
	id: string;
	
	constructor() {
		this.title = "";
		this.imgUrl = "";
		this.request = "";
		this.type = "column2d";
		this.id= "-1";
	}
}