import { Component, OnInit } from '@angular/core';

import { Favorite } from './favorite';
import { Diagram } from '../diagram/diagram';

import { DiagramService } from '../diagram/diagram.service';
import { ParseDslService } from '../services/parse-dsl.service';
import { FavoriteService } from './favorites.service';


@Component({
  selector: 'favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
 
export class FavoritesComponent{
	favoriteList: Favorite[];
	private isLoading: boolean = false;
	
	constructor(private diagramsService: DiagramService, private parseDslService: ParseDslService, private favoriteService: FavoriteService) {
		this.favoriteList = this.favoriteService.getFavoriteList();
	}
	
	generateFavoriteDiagram(favorite: Favorite) {
		if (this.isLoading) return;
		this.isLoading = true;
		this.parseDslService
			.query(favorite.request)
			.subscribe(data => {
				this.isLoading = false;
				this.diagramsService.add(data, favorite.request);
			});
	}
	
	removeFavorite(id: string) {
		this.favoriteService.removeFavorite(id);
	}
}