import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

import { Diagram } from '../diagram/diagram';
import { Favorite } from './favorite';


import { FAVORITE_LIST } from './mock-favorite';

@Injectable()
export class FavoriteService {
	private url = "http://ip94.ip-137-74-154.eu/bookmarks";
	
	/*
	créer favoris : bookmarks/create?bookmarkId=bookmarkId&accountId=accountId&title=title&query=query
	récup favoris : bookmarks/all?summoner=summonerName
	delete favoris : bookmarks/delete?id=bookmarkId
	*/
    currentDiagrams: Diagram[];

    constructor(private http: Http) {
		let user = window.localStorage.getItem("currentUser");
		if (user !== null) {
			this.getUserFavorites(user);
		}
	}
	
	getFavoriteList() : Favorite[] {
		return FAVORITE_LIST;
	}
	
	getUserFavorites(pseudo: string) {
		console.log("Je récupère mes favoris");
		let params = new URLSearchParams();
		params.set('summoner', pseudo);
		this.http.get(this.url + "/all", { search: params })
			.subscribe(datas => {
				let bookmarks = datas.json().bookmarks;
				for (let bookmark of bookmarks) {
					let favorite = new Favorite();
					let chartType = this.getChartType(bookmark.query);
					favorite.title = bookmark.title;
					favorite.request = bookmark.query;
					favorite.type = chartType;
					favorite.id = bookmark.id;
					favorite.imgUrl = this.getFavoriteIcon(favorite.type);
					
					FAVORITE_LIST.push(favorite);
				}
			console.log(FAVORITE_LIST);
			});
	}
	getChartType(query: string) : string {
		return query
				.substring(0, query.indexOf('chart ') + 6)
				.split(" ")
				.splice(-3)
				[0];
	}

	getFavoriteIcon(chartType: string): string {
		return "./assets/icon_" + chartType + "_chart.png";
	}
		
	parseQuery(request: string): string {
		return request.split("\n").join(" ");
	}
	
	addFavorite(d: Diagram) {
		console.log(d);
		let params = new URLSearchParams();
        params.set('bookmarkId', d.id + "");
        params.set('summonerName', window.localStorage.getItem("currentUser"));
        params.set('title', d.dataSource.chart.caption);
        params.set('query', this.parseQuery(d.request));
		
		this.http.get(this.url + "/create", { search: params })
			.subscribe(truc => {
				let favorite = new Favorite();

				favorite.title = d.dataSource.chart.caption;
				favorite.request = d.request;
				favorite.type = d.type;
				favorite.id = d.requestId;
				favorite.imgUrl = this.getFavoriteIcon(favorite.type);

				console.log(favorite);
				FAVORITE_LIST.push(favorite);
		});
		
		
	}
	
	removeFavorite(id: string) {
		let index = -1;
		
		for(let ii in FAVORITE_LIST) {
			
			if (FAVORITE_LIST[ii].id === id) {
				index = parseInt(ii);
				break;
			}
		}
		
		let params = new URLSearchParams();
		if (index !== -1) {
			params.set('id', id + "");
			this.http.get(this.url + "/delete", { search: params })
				.subscribe (truc => {
					console.log(truc);
					FAVORITE_LIST.splice(index, 1);
			});	
		}
	}
	
	isFavorite(diagram: Diagram): boolean {
		for(let favo of FAVORITE_LIST) {
			if (favo.id !== undefined && favo.request === diagram.request) {
				return true;
			}
		}
		return false;
	}
}