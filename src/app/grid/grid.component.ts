import { Component, OnInit } from '@angular/core';
import { DiagramService } from '../diagram/diagram.service';
import { Diagram } from '../diagram/diagram';
import { FavoriteService } from '../favorites/favorites.service';


@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent{
	diagramList = [];
	
    constructor(private diagramService: DiagramService, private favoriteService: FavoriteService) {
		this.diagramList = this.diagramService.getCurrentDiagrams();
	}
	
	deleteDiagram(d: any) {
		this.diagramService.deleteDiagram(d);
	}

    isInTwoColumns(diagram: any) {
        if (this.diagramList[this.diagramList.length-1] == diagram && 
            this.diagramList.length % 2 != 0)
            return false;
        return true;
    }
	
	updateFavoriteList(diagram: Diagram) {
		if(!this.isFavorite(diagram)) {
			this.favoriteService.addFavorite(diagram);	
		} /*else {
			this.favoriteService.removeFavorite(diagram.requestId, diagram.request);	
		}*/
		
	}
	
	isFavorite(diagram: Diagram): boolean {
		return this.favoriteService.isFavorite(diagram);
	}
}

