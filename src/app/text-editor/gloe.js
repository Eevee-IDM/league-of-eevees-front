ace.define("ace/mode/gloe_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(acequire, exports, module) {
"use strict";

var oop = acequire("../lib/oop");
var TextHighlightRules = acequire("./text_highlight_rules").TextHighlightRules;

var DocCommentHighlightRules = function() {
    this.$rules = {
        "start" : [ {
            token : "comment.doc.tag",
            regex : "@[\\w\\d_]+" // TODO: fix email addresses
        }, 
        DocCommentHighlightRules.getTagRule(),
        {
            defaultToken : "comment.doc",
            caseInsensitive: true
        }]
    };
};

var GloeHighlightRules = function() {
	var identifierRe = "[a-zA-Z\\$_\u00a1-\uffff][a-zA-Z\\d\\$_\u00a1-\uffff]*";
	var keywordMapper = this.createKeywordMapper({
			"keyword": "summoners|display|by|when|and|from|to|called|diagram|type|in"
			// "constant.language": "piou"
		}, "identifier");

	this.$rules = {
		"no_regex" : [
			{ token : keywordMapper, regex : identifierRe}
		],
		"start": [
			{token: "comment", regex: "\\/\\/.*$"},
			{token: "comment", regex: "\\/\\*", next : "comment"},
			{token: "string", regex: '["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]'},
			{token: "string", regex: "['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']"},
			{token: "constant.numeric", regex: "[+-]?\\d+(?:(?:\\.\\d*)?(?:[eE][+-]?\\d+)?)?\\b"},
			{ token : keywordMapper, regex : identifierRe}
		],
		"comment": [
			{token: "comment", regex: ".*?\\*\\/", next : "start"},
			{token: "comment", regex: ".+"}
		]
	};

    this.normalizeRules();
};

GloeHighlightRules.metaData = {
    fileTypes: ['gloe'],
    name: 'Gloe'
};

oop.inherits(GloeHighlightRules, TextHighlightRules);

exports.GloeHighlightRules = GloeHighlightRules;
});

ace.define("ace/mode/gloe",["require","exports","module","ace/lib/oop","ace/mode/text","ace/mode/gloe_highlight_rules"], function(acequire, exports, module) {
"use strict";

var oop = acequire("../lib/oop");
var TextMode = acequire("./text").Mode;
var GloeHighlightRules = acequire("./gloe_highlight_rules").GloeHighlightRules;

var Mode = function() {
    this.HighlightRules = GloeHighlightRules;
    this.$behaviour = this.$defaultBehaviour;
};
oop.inherits(Mode, TextMode);

(function() {
    this.lineCommentStart = "#";
    this.$id = "ace/mode/gloe";
}).call(Mode.prototype);

exports.Mode = Mode;
});
