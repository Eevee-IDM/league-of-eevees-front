import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { ParseDslService } from '../services/parse-dsl.service';
import { DiagramService } from '../diagram/diagram.service';

declare var require: any;

var xtext = require('../services/xtext/xtext-ace.js');
import 'brace/theme/twilight';

import 'brace';
import 'brace/ext/language_tools';
import './gloe';

@Component({
  selector: 'text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.css']
})
export class TextEditorComponent implements OnInit {
  editor: any;
  private errorMessage: string = '';
  private isLoading: boolean = false;

  constructor(private parseDslService: ParseDslService,
              private diagramsService: DiagramService) { }

  ngOnInit() {
    this.editor = xtext.createEditor({
      sendFullText: true,
      errorCallback: this.displayErrorMessage,
      context: this
    });
    this.editor.setTheme("ace/theme/twilight");
    this.editor.setValue(`summoners "${window.localStorage.getItem("currentUser")}" `, 1);
  }

  fetchDiagramData() {
    if (this.isLoading) return; // prevent the CTRL+shift shortcut when a diagram is already being generated
    this.isLoading = true;
    this.parseDslService
        .query(this.editor.getValue())
        .subscribe(data => {
          this.isLoading = false;
          data.code == 200 ?
            this.diagramsService.add(data, this.editor.getValue()) :
            this.displayErrorMessage(data.message);
        });
  }

  displayErrorMessage(message, context = null) {
    let that = context ? context : this;
    that.errorMessage = message;
    setTimeout(() => that.errorMessage = '', 10000);
  }

}
