import { Diagram } from './diagram';

export const DIAGRAM: any = {
	"chart": {
		"caption": "Best LOL champions",
		// "subCaption": "Top 5 champions in 2017 by popularity",
		"captionFontColor": "#add8e6",
		"subCaptionFontColor": "#86c5da",
		"baseFontColor": "#ffffff",
		"valueFontColor": "#ffffff",
		"numbersuffix": "%",
		"bgColor": "#141414",
		"bgAlpha": "100",
		"canvasbgColor": "#141414",
		"canvasbgAlpha": "100",
		"theme": "fint",
		"width": "100%",
		"height": "100%",
		"autoScale": "1"
	},
	"data": [
		{
			"label": "Ezreal",
			"value": "27"
		},
		{
			"label": "Caitlyn",
			"value": "25"
		},
		{
			"label": "Janna",
			"value": "18"
		},
		{
			"label": "Jhin",
			"value": "17"
		},
		{
			"label": "Kai'Sa",
			"value": "13"
		}
	]
};

export const DIAGRAM2: any = {
	"chart": {
	"caption": "Social Media Platforms Popularity",
	"subCaption": "2012-2016",
	"theme": "hulk-light",
	"showhovereffect": "1",
	"xAxisName": "Years",
	"yAxisName": "Popularity percentage",
	"showValues": "0",
	"numbersuffix": "%",
	"drawCrossLine": "1",
	"crossLineAlpha": "100",
	"crossLineColor": "#cc3300"
	},
	"categories": [{
		"category": [{
		"label": "2012"
		}, {
		"label": "2013"
		}, {
		"label": "2014"
		}, {
		"label": "2015"
		}, {
		"label": "2016"
		}
	]
	}],
	"dataset": [{
		"seriesname": "Facebook",
		"anchorBgColor": "#876EA1",
		"data": [
			{
				"value": "62"
			}, {
				"value": "64"
			}, {
				"value": "64"
			}, {
				"value": "66"
			}, {
				"value": "78"
			}
		]
		}, {
		"seriesname": "Instagram",
		"anchorBgColor": "#72D7B2",
		"data": [
			{
				"value": "16"
			}, {
				"value": "18"
			}, {
				"value": "24"
			}, {
				"value": "26"
			}, {
				"value": "32"
			}
		]
		}, {
		"seriesname": "LinkedIn",
		"anchorBgColor": "#77BCE9",
		"data": [
			{
				"value": "20"
			}, {
				"value": "22"
			}, {
				"value": "27"
			}, {
				"value": "22"
			}, {
				"value": "29"
			}
		]
		}, {
		"seriesname": "Twitter",
		"anchorBgColor": "#E5B556",
		"data": [
			{
				"value": "18"
			}, {
				"value": "19"
			}, {
				"value": "21"
			}, {
				"value": "21"
			}, {
				"value": "24"
			}
		]
	}]
}

export const DIAGRAM_LIST: any[] = [
	// {
	// 	id: 0,
	// 	type: 'pie2d',
	// 	width: '100%',
	// 	height: '75%',
	// 	dataFormat: 'json',
	// 	dataSource: DIAGRAM
	// }
]