export class Diagram {
	type: string;
	id: number;
	width: string;
	height: string;
	dataFormat: string;
	request: string;
	requestId: string;

	dataSource: any;
	
	isFavorite: boolean;
	
	constructor() {
		this.type = "";
		this.id = -1;
		this.width = "100%";
		this.height = "75%";
		this.dataFormat = "json";
		this.dataSource = {};
		this.isFavorite = false;
		this.request = "";
		this.requestId= "-1";
	}
}


/*
	Sur un point, ajouter l'attribut alpha (0-100) pour le mettre en transparent (comme opacity en css); -> multiple chart
	
	
summoners "Sar�nya"
display total of assists
in a bar chart called 'Top jungle'


*/
