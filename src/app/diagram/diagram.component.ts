import { Component } from '@angular/core';

import { DiagramService } from './diagram.service';

import { Diagram } from './diagram';

@Component({
  selector: 'diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.css']
})
export class DiagramComponent {

	diagram: Diagram = new Diagram();
	
	cpt = 1;
	
	diagramList = [];
	currentDiagrams = [];
	
    constructor(private diagramService: DiagramService) {
		this.diagramList = this.diagramService.getDiagramList();
		this.currentDiagrams = this.diagramService.getCurrentDiagrams();
	}

	displayOrHideDiagram(diagramID: number, diagramType: string, isMultiple: boolean = false) {
		let index = this.getIndexInDiagramList(diagramID);
		(index == -1) ?	
			this.diagramService.showDiagram(diagramID) :
			this.diagramService.hideDiagram(index);
	}

	getIndexInDiagramList(diagramID: number) {
		return this.currentDiagrams.findIndex(item => item.id === diagramID);
	}
	
	buildDiagram(diagramType: string) {
		this.diagram.id = this.cpt;
		this.diagram.width = '100%';
		this.diagram.height = '75%';
		this.diagram.type = diagramType;
		this.diagram.dataFormat = 'json';	
	}
	
	buildSingleDiagram(diagramType: string) {
		
		this.buildDiagram(diagramType);
        this.diagram.dataSource = this.diagramService.getDiagramData();
		
		this.diagramService.addDiagram(JSON.parse(JSON.stringify(this.diagram)));
		this.cpt ++;
    }
	
	deleteDiagram(index: number) : void {
		this.diagramList.splice(index, 1);
	}
}
