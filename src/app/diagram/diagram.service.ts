import { Injectable } from '@angular/core';
import { DIAGRAM, DIAGRAM2, DIAGRAM_LIST } from './mock-diagram';

import { Diagram } from './diagram';


@Injectable()
export class DiagramService {
    currentDiagrams: Diagram[];

    constructor() {
        this.currentDiagrams = [];
    }

    getDiagramData(): any {
        return DIAGRAM;
    }

	getDiagramList(): Diagram[] {
        console.log("All diagrams : ", DIAGRAM_LIST);
		return DIAGRAM_LIST;
    }

    getCurrentDiagrams() : Diagram[] {
        return this.currentDiagrams;
    }

    getDataSets(data: any) {
        return data.map(d => {
            return {
                seriesname: d.summoner,
                data: d.courbeSummoner.map(point => {
                    return {"value" : point.value}
                })
            }
        });
    }

    getCategoryIndices(indices) {
        return indices.map(i => {
            return {
                "label": i
            }
        })
    }
    
    add(data: any, request: string) : void {		
        console.log("I'm supposed to add the following diagram : ", data);
        // Do not add the diagram if it already exists
        // let alreadyExisting = DIAGRAM_LIST.filter(d => d.type == data.display.type && 
        //                                                d.dataSource.chart.caption == data.display.name)
        // if (alreadyExisting.length > 0)
        //     return;

        let diagram: Diagram = {
            type: (data.data.length > 1 ? 'ms' : '') + data.display.type,
            id: data.id,
            width: '100%',
            height: '75%',
            dataFormat: 'json',
			request : request,
			requestId : data.id + "",
            dataSource: {
                chart: {
                    caption: data.display.name == 'null' ? 'Unnamed diagram' : data.display.name,
                    captionFontColor: "#add8e6",
                    baseFontColor: "#ffffff",
                    valueFontColor: "#ffffff",
                    bgColor: "#141414",
                    bgAlpha: "100",
                    canvasbgColor: "#141414",
                    canvasbgAlpha: "100",
                    theme: "fint",
                    width: "100%",
                    height: "100%",
                    autoScale: "1",
                },
                categories: [{category : this.getCategoryIndices(data.indices)}],
                dataset: this.getDataSets(data.data)
            },
				isFavorite: false
        };

        console.log("New diagram : ", diagram);
		
        DIAGRAM_LIST.push(JSON.parse(JSON.stringify(diagram)));
        this.currentDiagrams.push(diagram);
    }

    // Not currently used
	addDiagram(d: Diagram) {
        DIAGRAM_LIST.push(d);
        this.currentDiagrams.push(d);
	}
	
	showDiagram(diagramID: number) {
        let diagram = DIAGRAM_LIST.filter(d => d.id == diagramID)[0];
		this.currentDiagrams.push(diagram);
	}
	
	hideDiagram(index: number) {
		this.currentDiagrams.splice(index, 1);
	}
	
	deleteDiagram(d: Diagram) {
		let index = DIAGRAM_LIST.indexOf(d);
		DIAGRAM_LIST.splice(index, 1);
	}
}
