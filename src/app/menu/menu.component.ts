import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
 
export class MenuComponent{
    @Input('showBurger') showBurger: boolean;
    @Output() onSwitch: EventEmitter<any> = new EventEmitter();

	showXtext: boolean = true;
	
    switchBurger() {
      this.showBurger = !this.showBurger;
      this.onSwitch.emit();
    }
}