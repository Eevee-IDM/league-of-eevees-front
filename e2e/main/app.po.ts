import { browser, by, element } from 'protractor';

export class AppPage {
  
  navigateTo() {
    return browser.get('/');
  }
  
  getTitleElement() {
    return element(by.id('dashboard-title'));
  }

  getTitle() {
    return this.getTitleElement().getText();
  }

  getLogOutButton() {
    return element(by.css('.log-out'));
  }

  logOut() {
    this.getLogOutButton().click();
  }

  getBurgerMenuIcon() {
    return element(by.css('.burger'));
  }

  toggleBurgerMenu() {
    this.getBurgerMenuIcon().click();
  }

  getTextEditor() {
    return element(by.id('xtext-editor'));
  }

  getAllDiagrams() {
    return element.all(by.css('.diag'));
  }

  getAllActiveButtons() {
    return element.all(by.css('.active-diagram'));
  }

  getActiveDiagramButton() {
    return element(by.css('.active-diagram'));
  }
}
