import { AppPage } from './app.po';
import { LandingPage } from '../landing-page/landing-page.po';
import { browser } from 'protractor';


describe('LoE App', () => {
  let page: AppPage;
  let landingPage: LandingPage;

  beforeEach(() => {
    page = new AppPage();
    landingPage = new LandingPage();
  });

  it('should not display the main page at first', () => {
    page.navigateTo();
    expect(page.getTitleElement().isPresent()).toBeFalsy();
  });

  it('should not display the login input after logging in', () => {
    landingPage.navigateTo();
    landingPage.logIn();
    expect(landingPage.getInputUsername().isDisplayed()).toBeFalsy();
    page.logOut();
  });

  it('should display the name of the app after logging in', () => {
    landingPage.navigateTo();
    landingPage.logIn();
    expect(page.getTitle()).toEqual('League Of Eevees');
    page.logOut();
  });

  it('should display the landing page after logging out', () => {
    page.navigateTo();
    landingPage.logIn();
    page.logOut();
    expect(landingPage.getInputUsername().isDisplayed()).toBeTruthy();
  });
});

describe('LoE App when logged in', () => {
  let page: AppPage;
  let landingPage: LandingPage;

  beforeEach(() => {
    page = new AppPage();
    landingPage = new LandingPage();
    page.navigateTo();
    browser.executeScript("return window.localStorage.getItem('currentUser');")
      .then(isLoggedIn => {if (!isLoggedIn) landingPage.logIn()})
  });

  it('should display the log out button', () => {
    expect(page.getLogOutButton().isPresent()).toBeTruthy();
  });

  it('should display the burger menu icon', () => {
    expect(page.getBurgerMenuIcon().isPresent()).toBeTruthy();
  });

  it('should hide the burger menu panel as default', () => {
    expect(page.getTextEditor().isPresent()).toBeFalsy();
  });

  it('should show the burger menu panel when clicking on the icon', () => {
    expect(page.getTextEditor().isPresent()).toBeFalsy();
    browser.waitForAngularEnabled(false); // prevent Angular timeout
    page.toggleBurgerMenu();
    expect(page.getTextEditor().isPresent()).toBeTruthy();
    browser.waitForAngularEnabled(true);
  });
  
  it('should display an active diagram by default', () => {
    expect(page.getActiveDiagramButton().isPresent()).toBeTruthy();
  });
  
  it('should display one default diagram', () => {
    expect(page.getAllDiagrams().count()).toEqual(1);
  });
  
  it('should display as many diagrams as selected in the button bar', () => {
    expect(page.getAllDiagrams().count())
      .toEqual(page.getAllActiveButtons().count());
  });
});
