import { AppPage } from '../main/app.po';
import { LandingPage } from '../landing-page/landing-page.po';
import { TextEditorPage } from './text-editor.po';
import { browser } from 'protractor';

describe('LoE Text Editor', () => {
  let appPage: AppPage;
  let textEditor: TextEditorPage;
  let landingPage: LandingPage;

  beforeEach(() => {
    appPage = new AppPage();
    landingPage = new LandingPage();
    textEditor = new TextEditorPage();

    appPage.navigateTo();
    browser.executeScript("return window.localStorage.getItem('currentUser');")
      .then(isLoggedIn => {if (!isLoggedIn) landingPage.logIn()})
    browser.waitForAngularEnabled(false); // prevent Angular timeout
  });

  afterEach(() => {
    browser.waitForAngularEnabled(true);
  });

  it('should display the summoner name in the text editor', () => {
    appPage.toggleBurgerMenu();
    expect(textEditor.getTextEditorContent())
        .toEqual(`summoners "Sarénya" `);
  });

  it('should hide the burger menu panel when double-clicking on the icon', () => {
    appPage.toggleBurgerMenu();
    appPage.toggleBurgerMenu();
    expect(textEditor.getTextEditor().isPresent())
        .toBeFalsy();
  });
});
