import { browser, by, element } from 'protractor';

export class TextEditorPage {

  getTextEditor() {
    return element(by.id('xtext-editor'));
  }

  getTextEditorContent() {
    return element(by.css('.ace_content')).getText();
  }
}
