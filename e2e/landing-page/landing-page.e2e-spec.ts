import { LandingPage } from './landing-page.po';
import { AppPage } from '../main/app.po';
import { browser } from 'protractor';

describe('LoE LandingPage', () => {
  let page: LandingPage;
  let appPage: AppPage;

  beforeEach(() => {
    page = new LandingPage();
    appPage = new AppPage();
  });

  it('should display the name of the application', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('League Of Eevees');
  });

  it('should display an input field for logging in', () => {
    page.navigateTo();
    expect(page.getInputUsername().isPresent())
          .toEqual(true);
  });

  it('should display the dashboard when the user is logged in', () => {
    page.navigateTo();
    page.logIn();
    expect(appPage.getTitle())
      .toEqual('League Of Eevees');
    appPage.logOut();
  });
});
