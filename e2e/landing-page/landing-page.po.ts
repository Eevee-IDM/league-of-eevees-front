import { browser, by, element } from 'protractor';

export class LandingPage {
  private username: string = 'Sarénya';
  
  navigateTo() {
    return browser.get('/');
  }

  getTitle() {
    return element(by.id('landing-title')).getText();
  }

  getInputUsername() {
    return element(by.name('pseudo'));
  }

  logIn() {
    element(by.name('pseudo')).sendKeys(this.username);
    element(by.css('.log-in')).click();
  }
}